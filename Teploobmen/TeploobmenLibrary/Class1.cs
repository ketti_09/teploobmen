﻿namespace TeploobmenLibrary
{
    public class Teploobmen
    {
        public double h;//Высота слоя (м)
        public double t1;//нач. температура окатышей (градусы)
        public double T1;//нач. температура воздуха (градусы)
        public double s;//Скорость воздуха на свободное сечение шахты (м/с)
        public double C;//Средняя теплоемкость воздух при 200°С (кДж/(кг*К))
        public double Cm;//Расход окатышей (кг/с)
        public double Gm;//Теплоемкость окатышей (кДж/(кг • К))
        public double aV;//Объемный коэффициент теплоотдачи (Вт/(м3 • К))
        public double d;//Диаметр аппарата (м)
        public Teploobmen(TeploobmenInput input)
        {
            h = input.h;
            t1 = input.t1;
            T1 = input.T;
            s = input.s;
            C = input.C;   
            Cm= input.Cm;
            Gm= input.Gm;
            aV = input.aV;  
            d = input.d;
        }
        public TeploobmenOutput calc()
        {
            double S = 3.14 * ((d / 2) * (d / 2));
            double m = (Gm * Cm) / (s * C * S);
            double Y0 = (h * aV * S) / (s * C * S * 1000);
            double form1 = 1 - m * Math.Exp(((m - 1) * Y0) / m);
            int j;
            List<double> k = new List<double>();
            for (double i = 0; i <= h; i = i + 0.5)
            {
                k.Add(0 + i);
            }
            List<double> y = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                y.Add(Math.Round((((aV * k[j]) / (s * C * 1000))), 2));
            }
            List<double> X = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                X.Add(Math.Round(1 - Math.Exp(((m - 1) * y[j]) / m), 2));
            }
            List<double> Z = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                Z.Add(Math.Round(1 - m * Math.Exp(((m - 1) * y[j]) / m), 2));
            }
            List<double> v = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                v.Add(Math.Round((X[j] / form1), 2));
            }
            List<double> o = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                o.Add(Math.Round((Z[j] / form1), 2));
            }

            List<double> t = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                t.Add(Math.Round(Math.Abs(((T1 - t1) * v[j]) + t1), 0));
            }
            List<double> T = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                T.Add(Math.Round(Math.Abs(((T1 - t1) * o[j]) + t1), 0));
            }
            List<double> d_t = new List<double>();
            for (j = 0; j < k.Count; j++)
            {
                d_t.Add(Math.Round(Math.Abs(t[j] - T[j]), 2));
            }
            return new TeploobmenOutput
            {
                square = Math.Round(S, 4),
                relationship = Math.Round(m, 4),
                full_height = Math.Round(Y0, 4),
                position = k.GetRange(0, k.Count),
                Y = y.GetRange(0, y.Count),
                X = X.GetRange(0, X.Count),
                Z = Z.GetRange(0, Z.Count),
                V = v.GetRange(0, v.Count),
                O = o.GetRange(0, o.Count),
                t = t.GetRange(0, t.Count),
                T = T.GetRange(0, T.Count),
                diff_temp = d_t.GetRange(0, d_t.Count)
            };
        }
    }
}