﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeploobmenLibrary
{
    public class TeploobmenInput
    {
        public double h { get; set; }   //Высота слоя (м)
        public double t1 { get; set; }  //Начальная температура материала (градусы)
        public double T { get; set; }   //Начальная температура воздуха (градусы)
        public double s { get; set; }   //Скорость воздуха на свободное сечение шахты (м/с)
        public double C { get; set; }   //Средняя теплоемкость воздух при 200°С (кДж/(кг*К))
        public double Cm { get; set; }  //Расход окатышей (кг/с)
        public double Gm { get; set; }  //Теплоемкость окатышей (кДж/(кг • К))
        public double aV { get; set; }  //Объемный коэффициент теплоотдачи (Вт/(м3 • К))
        public double d { get; set; }   //Диаметр аппарата (м)
        public string? Name { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; }
    }
}
