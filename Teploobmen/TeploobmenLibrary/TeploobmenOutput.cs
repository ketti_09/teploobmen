﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeploobmenLibrary
{
    public class TeploobmenOutput
    {
        public double square { get; set; }  //площадь основания S кв.метр
        public double relationship { get; set; }    //Отношение теплоемкости потоков m
        public double full_height { get; set; } //Полная относитиельная высота слоя Y0
        public List<double> position { get; set; }
        public List<double> Y { get; set; }
        public List<double> X { get; set; }
        public List<double> Z { get; set; }
        public List<double> V { get; set; }
        public List<double> O { get; set; }
        public List<double> t { get; set; } 
        public List<double> T { get; set; } 
        public List<double> diff_temp { get; set; } //Разность температур (градусы)
    }
}
