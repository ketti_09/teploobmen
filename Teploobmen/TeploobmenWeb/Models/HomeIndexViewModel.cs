﻿using Microsoft.AspNetCore.Mvc;
using TeploobmenWeb.Data;

namespace TeploobmenWeb.Models
{
    public class HomeIndexViewModel 
    {
        public Variant? Variant { get; set; }
        public List<Variant> Variants { get; set; }
    }
}
